#![allow(dead_code, unused_variables)]
#![feature(map_first_last)]
pub mod leetcode;
pub mod util;
mod snippet;

pub mod codewar;
pub mod advcode2020;

pub mod topic1_array;
pub mod topic2_graph;
pub mod cpp_algorithms;
